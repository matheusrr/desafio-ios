//
//  ViewController.swift
//  GitHub Java
//
//  Created by Matheus on 10/08/16.
//  Copyright © 2016 Fives. All rights reserved.
//

import UIKit
import AFNetworking
import ModelRocket
import SDWebImage

class ViewController: UITableViewController {

    var totalElementos:Int?
    var repositorios:NSMutableArray = NSMutableArray()
    var pagRepositorios = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        carregarRepositorios()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let selectedCell:UITableViewCell = sender as! UITableViewCell
        let autor:Usuario = (repositorios.objectAtIndex(tableView.indexPathForCell(selectedCell)!.row) as! RepositorioGit).usuario.value!
        (segue.destinationViewController as! Repositorio).nomeRepositorio = (selectedCell.viewWithTag(1) as! UILabel).text
        (segue.destinationViewController as! Repositorio).autor = autor
    }
    
    func carregarRepositorios() -> Void {
        let urlRepositorios:NSURL = NSURL.init(string: "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(pagRepositorios)")!
        let manager:AFHTTPSessionManager = AFHTTPSessionManager()
        manager.GET(urlRepositorios.absoluteString, parameters: nil, progress: nil, success: { (task, responseObject) in
//            print("JSON: \(responseObject)")
            if responseObject!.isKindOfClass(NSDictionary) {
                self.processaDadosServidor(JSON(responseObject!))
            }
            }) { (operation, error) in
                print("Error: \(error)")
                let alert = UIAlertController(title: "Error", message:"Error \(error.code)", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func processaDadosServidor(json:JSON) -> Void {
        let auxRetorno = RetornoServidor(json: json)
        totalElementos = auxRetorno.total.value
        if auxRetorno.itens.count > 0 {
            repositorios.addObjectsFromArray(auxRetorno.itens.values)
        }
        tableView.reloadData()
    }
    
    func carregarDadosExtras(cell:UITableViewCell, index:Int) -> Void {
        let auxRespositorio:RepositorioGit = repositorios.objectAtIndex(index) as! RepositorioGit
        let urlRepositorios:NSURL = NSURL.init(string: auxRespositorio.usuario.value!.urlName.value!)!
        let manager:AFHTTPSessionManager = AFHTTPSessionManager()
        manager.GET(urlRepositorios.absoluteString, parameters: nil, progress: nil, success: { (task, responseObject) in
            if responseObject!.isKindOfClass(NSDictionary) {
                auxRespositorio.usuario.value = Usuario(json: JSON(responseObject!))
                dispatch_async(dispatch_get_main_queue(), {
                    (cell.viewWithTag(7) as! UILabel).text = auxRespositorio.usuario.value!.nome.value
                    (cell.viewWithTag(5) as! UIImageView).sd_setImageWithURL(NSURL.init(string: auxRespositorio.usuario.value!.urlFoto.value!)!)
                })
            }
        }) { (operation, error) in
            print("Error: \(error)")
//            let alert = UIAlertController(title: "Error", message:"Error \(error.code)", preferredStyle: UIAlertControllerStyle.Alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    //Delegate e DataSource
    
    override func scrollViewWillBeginDecelerating(scrollView: UIScrollView) {
        let offset:CGPoint  = scrollView.contentOffset
        let bounds:CGRect = scrollView.bounds
        let  size:CGSize = scrollView.contentSize
        let inset:UIEdgeInsets  = scrollView.contentInset
        let y:Float = Float(offset.y + bounds.size.height - inset.bottom)
        let h:Float = Float(size.height)
        
        let reload_distance:Float = 50
        if(y > h + reload_distance) {
            if repositorios.count < totalElementos {
                pagRepositorios = pagRepositorios + 1
                carregarRepositorios()
            }
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositorios.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("celula", forIndexPath: indexPath)
        /* Index de Tags
         1 - Nome
         2 - Descrição
         3 - Forks
         4 - Estrelas
         5 - Foto do Autor
         6 - Login do Autor
         7 - Nome do Autor
         */
        if indexPath.row < repositorios.count {
            let auxRespositorio:RepositorioGit = repositorios.objectAtIndex(indexPath.row) as! RepositorioGit
            (cell.viewWithTag(1) as! UILabel).text = auxRespositorio.nome.value
            (cell.viewWithTag(2) as! UILabel).text = auxRespositorio.descricao.value
            (cell.viewWithTag(3) as! UILabel).text = String.init(format: "%d", auxRespositorio.forks.value!)
            (cell.viewWithTag(4) as! UILabel).text = String.init(format: "%d", auxRespositorio.estrelas.value!)
            (cell.viewWithTag(6) as! UILabel).text = auxRespositorio.usuario.value!.username.value
            self.carregarDadosExtras(cell, index: indexPath.row)
        }
        return cell
    }
    
}

