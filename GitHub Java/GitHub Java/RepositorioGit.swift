//
//  RepositorioGit.swift
//  GitHub Java
//
//  Created by Matheus on 11/08/16.
//  Copyright © 2016 Fives. All rights reserved.
//

import Foundation
import ModelRocket

extension RepositorioGit: JSONTransformable {}

class RepositorioGit: Model {
    let nome = Property<String>(key: "name")
    let descricao = Property<String>(key: "description")
    var usuario = Property<Usuario>(key: "owner")
    let forks = Property<Int>(key: "forks")
    let estrelas = Property<Int>(key: "stargazers_count")
}