//
//  PullRequest.swift
//  GitHub Java
//
//  Created by Matheus on 12/08/16.
//  Copyright © 2016 Fives. All rights reserved.
//

import Foundation
import ModelRocket

extension PullRequest: JSONTransformable {}

class PullRequest: Model {
    let titulo = Property<String>(key: "title")
    let url = Property<String>(key: "html_url")
    let body = Property<String>(key: "body")
    var usuario = Property<Usuario>(key: "user")
    var data:NSDate?
}