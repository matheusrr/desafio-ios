//
//  RetornoServidor.swift
//  GitHub Java
//
//  Created by Matheus on 11/08/16.
//  Copyright © 2016 Fives. All rights reserved.
//

import Foundation
import ModelRocket

class RetornoServidor: Model {
    let total = Property<Int>(key: "total_count")
    var itens = PropertyArray<RepositorioGit>(key: "items")
}