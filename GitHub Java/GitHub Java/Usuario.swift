//
//  Usuario.swift
//  GitHub Java
//
//  Created by Matheus on 11/08/16.
//  Copyright © 2016 Fives. All rights reserved.
//

import Foundation
import ModelRocket

extension Usuario: JSONTransformable {}

class Usuario: Model {
    let nome = Property<String>(key: "name")
    let urlName = Property<String>(key: "url")
    let urlFoto = Property<String>(key: "avatar_url")
    let username = Property<String>(key: "login")
}

