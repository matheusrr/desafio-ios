//
//  Repositorio.swift
//  GitHub Java
//
//  Created by Matheus on 11/08/16.
//  Copyright © 2016 Fives. All rights reserved.
//

import UIKit
import AFNetworking
import ModelRocket
import SDWebImage

class Repositorio: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var nomeRepositorio:String?
    var totalElementos:Int?
    var pulls:NSMutableArray = NSMutableArray()
    var pagPull = 1
    var autor:Usuario?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dispatch_async(dispatch_get_main_queue(), {
            self.title = self.nomeRepositorio
        })
        carregarPulls()
    }
    
    func carregarPulls() -> Void {
        let urlRepositorios:NSURL = NSURL.init(string: String.init(format: "https://api.github.com/repos/%@/%@/pulls?page=%d", autor!.username.value!, nomeRepositorio!, pagPull))!
        let manager:AFHTTPSessionManager = AFHTTPSessionManager()
        manager.GET(urlRepositorios.absoluteString, parameters: nil, progress: nil, success: { (task, responseObject) in
            //            print("JSON: \(responseObject)")
            if responseObject!.isKindOfClass(NSArray) {
                self.processaDadosServidor(responseObject! as! NSArray)
            }
        }) { (operation, error) in
            print("Error: \(error)")
            let alert = UIAlertController(title: "Error", message:"Error \(error.code)", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func processaDadosServidor(array:NSArray) -> Void {
        for item in array {
            let auxRetorno:PullRequest = PullRequest(json: JSON(item))
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            auxRetorno.data = dateFormatter.dateFromString((item as! NSDictionary).valueForKey("created_at") as! String)
            
            pulls.addObject(auxRetorno)
        }
        tableView.reloadData()
    }
    
    func carregarDadosExtras(cell:UITableViewCell, index:Int) -> Void {
        let auxPull:PullRequest = pulls.objectAtIndex(index) as! PullRequest
        let urlUsuario:NSURL = NSURL.init(string: auxPull.usuario.value!.urlName.value!)!
        let manager:AFHTTPSessionManager = AFHTTPSessionManager()
        manager.GET(urlUsuario.absoluteString, parameters: nil, progress: nil, success: { (task, responseObject) in
            if responseObject!.isKindOfClass(NSDictionary) {
                auxPull.usuario.value = Usuario(json: JSON(responseObject!))
                dispatch_async(dispatch_get_main_queue(), {
                    (cell.viewWithTag(5) as! UILabel).text = auxPull.usuario.value!.nome.value
                    (cell.viewWithTag(3) as! UIImageView).sd_setImageWithURL(NSURL.init(string: auxPull.usuario.value!.urlFoto.value!)!)
                })
            }
        }) { (operation, error) in
            print("Error: \(error)")
//            let alert = UIAlertController(title: "Error", message:"Error \(error.code)", preferredStyle: UIAlertControllerStyle.Alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    //Delegate e DataSource
    
    func scrollViewWillBeginDecelerating(scrollView: UIScrollView) {
        let offset:CGPoint  = scrollView.contentOffset
        let bounds:CGRect = scrollView.bounds
        let  size:CGSize = scrollView.contentSize
        let inset:UIEdgeInsets  = scrollView.contentInset
        let y:Float = Float(offset.y + bounds.size.height - inset.bottom)
        let h:Float = Float(size.height)
        
        let reload_distance:Float = 50
        if(y > h + reload_distance) {
            pagPull = pagPull + 1
            carregarPulls()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pulls.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("celula", forIndexPath: indexPath)
        /* Index de Tags
         1 - Nome
         2 - Descrição
         3 - Foto do Autor
         4 - Login do Autor
         5 - Nome do Autor
         6 - Data
         */
        if indexPath.row < pulls.count {
            let auxPull:PullRequest = pulls.objectAtIndex(indexPath.row) as! PullRequest
            (cell.viewWithTag(1) as! UILabel).text = auxPull.titulo.value
            (cell.viewWithTag(2) as! UILabel).text = auxPull.body.value
            (cell.viewWithTag(4) as! UILabel).text = auxPull.usuario.value!.username.value
            
            if auxPull.data != nil {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                let strDate:String? = dateFormatter.stringFromDate(auxPull.data!)
                (cell.viewWithTag(6) as! UILabel).text = strDate
            }
            
            self.carregarDadosExtras(cell, index: indexPath.row)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        let auxPull:PullRequest = pulls.objectAtIndex(indexPath.row) as! PullRequest
        UIApplication.sharedApplication().openURL(NSURL.init(string: auxPull.url.value!)!)
        return nil
    }
}
